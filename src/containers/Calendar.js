import { connect } from 'react-redux';
import Calendar from '../components/Calendar';
import * as module from '../modules/navi';

const mapStateToProps = (state, ownProps) => {
  return state;
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    selectDate: (dayOfMonth) => {dispatch(module.selectDate(dayOfMonth))}
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
