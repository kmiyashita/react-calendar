// @flow

// this file contains many code snippets testing flow type checking mechanism.

import navi from './navi';
import React from 'react';

// originally the following line caused an error.
// the error is resolved by introducing flow-typed.
import { combineReducers } from 'redux';

//////////////////////////////////////
// entry points
//////////////////////////////////////

/**
  * this function is called when you click 'test' button.
  */
export const testFlow = () => {
  testSimple();
}

//////////////////////////////////////
// simple samples
//////////////////////////////////////

const testSimple = () => {
  const z = foo(1, 4);
  console.log(`foo returns ${z}`);

  // the following codes causes flow error.
  // const w = foo('abc', 1);
  // console.log(`foo returns ${w}`);
}

function foo(x : number, y : number) {
  return (x + y);
}


//////////////////////////////////////
// generics
// https://flow.org/en/docs/types/generics/
//////////////////////////////////////

type IdentityWrapper = {
  func<T>(T): T
}

function identity(value) {
  return value;
}

function stringIdentity(value: string): string {
  return value;
}

function genericIdentity<T>(value: T): T {
  return value;
}

// aha... even if string can be interpreted an instance of T,
// stringIdentity function cannot be used in this case.
// const bad: IdentityWrapper = { func: identity }; // Error!
// const bad2: IdentityWrapper = { func: stringIdentity }; // Error!
const good: IdentityWrapper = { func: genericIdentity }; // Works!

// Function types with generics

// tihs must be the same as IdentityWrapper
type IdentityWrapper2 = {
  func : <T>(param:T) => T;
}

// const bad3: IdentityWrapper2 = { func: stringIdentity }; // Error!
const good2 : IdentityWrapper2 = {func : genericIdentity};  // Works!


//////////////////////////////////////
// redux with type info
// see redux_v4.x.x.js
//////////////////////////////////////
