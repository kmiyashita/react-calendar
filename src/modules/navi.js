import * as CalApi from './access-calendar.js'

// Actions
const INIT_GOOGLE_API = 'react-calendar/navi/INIT_GOOGLE_API';
const SIGN_IN    = 'react-calendar/navi/SIGN_IN';
const SIGN_OUT   = 'react-calendar/navi/SIGN_OUT';
const LOAD_MONTH = 'react-calendar/navi/LOAD_MONTH';
const SELECT_DATE = 'react-calendar/navi/SELECT_DATE';

// Action creators

/**
  * initialize google API, using redux-thunk
  */
export const initGoogleApi = () => {
  return (dispatch, getState) => {
    CalApi.initialize()
    .then(() => {
      dispatch({type: INIT_GOOGLE_API});
      if (CalApi.isSignedIn()) {
        dispatch({type: SIGN_IN});
      }
      loadMonth(dispatch, getState);
    })
    .catch(() => {
      console.log('google API is already initialized.');
    });
  }
}

/**
  * Sign in, using redux-thunk
  */
export const signIn = () => {
  return (dispatch, getState) => {
    CalApi.signIn()
    .then((signedIn) => {
      dispatch({type: SIGN_IN});
      loadMonth(dispatch, getState);
    })
    .catch(() => {
      console.log('signing in fails.');
    });
  }
}

/**
  * Sign out, using redux-thunk
  */
export const signOut = () => {
  return (dispatch, getState) => {
    CalApi.signOut()
    .then(() => {
      dispatch({type: SIGN_OUT});
      loadMonth(dispatch, getState);
    })
    .catch(() => {
      console.log('signing out fails.');
    });
  }
}

/**
  * Go to the previous month.
  * Retrieve the events of the month, using redux-thunk.
  */
export const prevMonth = () => {
  return (dispatch, getState) => {
    const state = getState();
    let year = state.navi.year;
    let month = state.navi.month;

    if(month > 0) {
      month--;
    } else {
      year--;
      month = 11;
    }

    loadMonth(dispatch, getState, year, month);
  };
}

/**
  * Go to the next month.
  * Retrieve the events of the month, using redux-thunk.
  */
export const nextMonth = () => {
  return (dispatch, getState) => {
    const state = getState();
    let year = state.navi.year;
    let month = state.navi.month;

    if(month < 11) {
      month++;
    } else {
      year++;
      month = 0;
    }

    loadMonth(dispatch, getState, year, month);
  };
}

/**
  * Set the current month, and load events of the month.
  * @param {func} dispatch  dispatch function
  * @param {func} getState  getState function
  * @param {int}  year      year. -1 means 'keep the current year'
  * @param {int}  month     month (0:Jan, 1:Feb...). -1 means 'keep the current month'
  */
const loadMonth = (dispatch, getState, year=-1, month=-1) => {
  const state = getState();
  if(year === -1) {
    year = state.navi.year;
  }
  if(month === -1) {
    month = state.navi.month;
  }
  if(state.navi.isSignedIn) {
    CalApi.listMonthEvents(year, month)
    .then((eventArray) => {
      dispatch({type: LOAD_MONTH, year: year, month: month,
                events:eventArray});
    });
  } else {
    dispatch({type: LOAD_MONTH, year: year, month: month,
              events:emptyEvents});
  }
}

/**
  * Select a certain date.
  *
  * @param {int}  dayOfMonth  the day of the month (1-31) or 0 when empty.
  */
export const selectDate = (dayOfMonth) => {
  return {type: SELECT_DATE, dayOfMonth};
}


// Reducer
const now = new Date();
const emptyEvents = new Array(32);
for(let i = 0; i < 32; i++){
  emptyEvents[i] = [];
}

const initialState = {
  isGoogleApiReady: false,
  isSignedIn: false,
  year:  now.getFullYear(),
  month: now.getMonth(),
  dayOfMonth: now.getDate(),
  events: emptyEvents
};

const reducer = (state = initialState, action = {}) => {
  switch (action.type) {

    case INIT_GOOGLE_API:
      return Object.assign({}, state, {isGoogleApiReady: true});

    case SIGN_IN:
      return Object.assign({}, state, {isSignedIn: true});

    case SIGN_OUT:
      return Object.assign({}, state, {isSignedIn: false});

    case LOAD_MONTH:
      return Object.assign({}, state,
              {year: action.year, month: action.month, dayOfMonth: 0,
               events: action.events});

    case SELECT_DATE:
      return Object.assign({}, state, {dayOfMonth: action.dayOfMonth});

    default:
      return state;
  }
}

export default reducer;
