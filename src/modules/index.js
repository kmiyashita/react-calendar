import { combineReducers } from 'redux';
import navi from './navi';

// Reducer
// We have currently only one reducer. Let me examine how combineReducers works.
const reducer = combineReducers({
  navi
});

export default reducer;
