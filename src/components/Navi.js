import React from 'react'

class Navi extends React.Component {
  componentDidMount() {
    this.props.initGoogleApi();
  }

  render() {
    return (
      <div>
        <button onClick={this.props.prevMonth}>Prev</button>
        {'' + this.props.navi.year + '-' + (this.props.navi.month + 1)}
        <button onClick={this.props.nextMonth}>Next</button>
        <div>
          {this.props.navi.isSignedIn ?
            <button onClick={this.props.signOut}>Sign Out</button> :
            <button onClick={this.props.signIn}>Sign In</button>}
        </div>
        <button onClick={this.props.testFlow}>Test</button>
      </div>
    );
  }
}

export default Navi;
